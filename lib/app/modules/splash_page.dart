import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:lottie/lottie.dart';

class SplashPage extends StatefulWidget {
  final String title;
  const SplashPage({Key? key, this.title = 'SplashPage'}) : super(key: key);
  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      Container(color: Colors.black),
      Positioned(
          top: 0,
          right: -30,
          child: Lottie.asset('assets/animations/splash_fasttrade.json',
              repeat: false,
              height: MediaQuery.of(context).size.height,
              fit: BoxFit.cover, onLoaded: (_) {
            Timer(Duration(milliseconds: 2500),
                () => Modular.to.pushReplacementNamed('/auth/'));
          }))
    ]));
  }
}
