import 'package:flutter_mobx_sample/app/shared/components/button_widget.dart';
import 'package:flutter_mobx_sample/app/shared/components/text_field_widget.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_mobx_sample/app/modules/auth/auth_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AuthPage extends StatefulWidget {
  final String title;
  const AuthPage({Key? key, this.title = 'AuthPage'}) : super(key: key);

  @override
  AuthPageState createState() => AuthPageState();
}

class AuthPageState extends ModularState<AuthPage, AuthStore> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => store.getUserIP());
  }

  Widget _getHeader() {
    return Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top, left: 16, right: 16),
        child: Row(children: [
          Expanded(
              child: Text('Versão 3.2.0',
                  style: TextStyle(color: Colors.white, fontSize: 12))),
          Directionality(
              textDirection: TextDirection.rtl,
              child: TextButton.icon(
                  onPressed: () {},
                  icon: SvgPicture.asset('assets/icons/icon_help.svg'),
                  label: Text(
                    "Ajuda",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  )))
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF090a0c),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: SingleChildScrollView(
                child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height,
                    ),
                    child: IntrinsicHeight(
                        child: Stack(children: [
                      SvgPicture.asset('assets/images/img_auth_background.svg',
                          fit: BoxFit.fill,
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width),
                      _getHeader(),
                      Container(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset('assets/images/img_logo.svg'),
                              SizedBox(height: 24),
                              Text(
                                'Mais liberdade para investir de qualquer lugar, com agilidade e sem burocracia',
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 32),
                              TextFieldWidget(
                                  title: "Conta/Usuário",
                                  keyboardType: TextInputType.emailAddress,
                                  maxLength: 100,
                                  onChanged: store.setUserName),
                              SizedBox(height: 24),
                              TextFieldWidget(
                                  title: "Senha",
                                  keyboardType: TextInputType.emailAddress,
                                  maxLength: 100,
                                  isPassword: true,
                                  onChanged: store.setPassword),
                              Container(
                                  alignment: Alignment.centerRight,
                                  child: TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        'Esqueci minha senha',
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.blue),
                                        textAlign: TextAlign.end,
                                      ))),
                              SizedBox(height: 24),
                              ButtonWidget(
                                  title: "ACESSAR", onPressed: store.onSubmit),
                              SizedBox(height: 24),
                              Container(
                                  alignment: Alignment.center,
                                  child: TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        'AINDA NÃO POSSUO CONTA',
                                        style: TextStyle(
                                            fontSize: 16, color: Colors.white),
                                        textAlign: TextAlign.end,
                                      ))),
                            ],
                          ))
                    ]))))));
  }
}
