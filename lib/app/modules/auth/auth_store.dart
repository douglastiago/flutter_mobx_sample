import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_mobx_sample/app/shared/repositories/auth_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'auth_store.g.dart';

class AuthStore = _AuthStoreBase with _$AuthStore;

abstract class _AuthStoreBase with Store {
  final AuthRepository api = Modular.get();

  @observable
  String? userName;

  @observable
  String? password;

  @observable
  String? errorUserName;

  @observable
  String? errorPassword;

  String? userIP;

  @action
  setUserName(String value) => userName = value;

  @action
  setPassword(String value) => password = value;

  @computed
  bool get canSubmit =>
      (userName ?? '').isNotEmpty && (password ?? '').isNotEmpty;

  @action
  void validateUserName() {
    errorUserName = (userName ?? '').isEmpty ? "Campo obrigatório." : null;
  }

  @action
  void validatePassword() {
    errorPassword = (password ?? '').isEmpty ? 'Campo obrigatório.' : null;
  }

  getUserIP() async {
    final response = await api.getUserIP();
    if (!response.error) userIP = response.data?.ip;
  }

  signIn() async {
    EasyLoading.show();
    var data = {
      "username": userName,
      "password": password,
      "softwareName": "FT_ANDROID_CEDRO",
      "softwareVersion": "3.2.0",
      "ipAdress": userIP
    };

    final response = await api.signIn(data);
    EasyLoading.dismiss();
    if (response.error || (response.data?.code ?? '').isNotEmpty) {
      if ((response.data?.message ?? '').isNotEmpty) {
      } else {}
    } else {
      Modular.to.pushReplacementNamed('/home/');
    }
  }

  getContextUserInfo() async {
    EasyLoading.show();
    var data = {"usermame": userName, "password": password};
    final response = await api.getContextInfoUser(data);
    EasyLoading.dismiss();
    if (response.error) {
    } else {
      var shadowActive = response.data?.shadow?.shadowActive;
      switch (shadowActive) {
        case "1":
          var markets = response.data?.permissions?.markets;
          var software = response.data?.permissions?.softwares
              ?.firstWhere((element) => element.softwareCode == 101);
          if (software == null) {
          } else if (markets == null || markets.isEmpty) {
          } else {
            signIn();
          }
          break;
        case "0":
          break;
        default:
          break;
      }
    }
  }

  onSubmit() async {
    validateUserName();
    validatePassword();

    if (canSubmit) getContextUserInfo();
  }
}
