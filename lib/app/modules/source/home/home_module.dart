import 'package:flutter_mobx_sample/app/modules/source/home/home_Page.dart';
import 'package:flutter_mobx_sample/app/modules/source/home/home_store.dart';
import 'package:flutter_mobx_sample/app/modules/source/home/pages/internal_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => HomeStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => HomePage(), children: [
      ChildRoute('/wallet',
          child: (_, args) => InternalPage(
                title: 'Carteira',
              ),
          transition: TransitionType.noTransition),
      ChildRoute('/order',
          child: (_, args) => InternalPage(title: 'Ordens'),
          transition: TransitionType.noTransition),
      ChildRoute('/quotation',
          child: (_, args) => InternalPage(title: 'Cotações'),
          transition: TransitionType.noTransition),
      ChildRoute('/negotiation',
          child: (_, args) => InternalPage(title: 'Negociações'),
          transition: TransitionType.noTransition),
      ChildRoute('/menu',
          child: (_, args) => InternalPage(title: 'Menu'),
          transition: TransitionType.noTransition),
    ]),
  ];
}
