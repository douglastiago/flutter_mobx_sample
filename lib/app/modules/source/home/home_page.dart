import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_mobx_sample/app/modules/source/home/home_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key? key, this.title = 'HomePage'}) : super(key: key);
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends ModularState<HomePage, HomeStore> {
  @override
  void initState() {
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    WidgetsBinding.instance?.addPostFrameCallback(
        (_) => {store.setIndex(2), Modular.to.navigate('/home/quotation')});
  }

  final items = [
    NavigationBarItem('assets/icons/icon_wallet.svg',
        title: "Carteira", index: 0),
    NavigationBarItem('assets/icons/icon_order.svg', title: "Ordens", index: 1),
    NavigationBarItem('assets/icons/icon_quotation.svg',
        title: "Cotação", index: 2),
    NavigationBarItem('assets/icons/icon_negotiation.svg',
        title: "Negociar", index: 3),
    NavigationBarItem('assets/icons/icon_more.svg', title: "Mais", index: 4)
  ];

  Widget _navigationBarItem(String icon,
      {String? title, bool isSelected = false, Function()? onPressed}) {
    return Expanded(
        child: InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 2),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Icon(icon, color: ),
            SvgPicture.asset(icon,
                color: isSelected ? Colors.blue : Color(0xff21262E)),
            SizedBox(height: 4),
            Text(title ?? '',
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: isSelected ? Colors.blue : Color(0xff21262E)))
          ],
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: Colors.white,
      body: RouterOutlet(),
      bottomNavigationBar: Container(
          decoration: BoxDecoration(color: Colors.transparent, boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              spreadRadius: 0,
              blurRadius: 25,
              offset: Offset(0, 4),
            ),
          ]),
          child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8),
              ),
              child: BottomAppBar(
                  shape: CircularNotchedRectangle(),
                  child: Observer(
                      builder: (_) => Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: items
                              .map((item) => _navigationBarItem(
                                    item.icon,
                                    title: item.title,
                                    onPressed: () {
                                      store.setIndex(item.index ?? 2);
                                      switch (item.index) {
                                        case 0:
                                          Modular.to.navigate('/home/wallet');
                                          break;
                                        case 1:
                                          Modular.to.navigate('/home/order');
                                          break;
                                        case 3:
                                          Modular.to
                                              .navigate('/home/negotiation');
                                          break;
                                        case 4:
                                          Modular.to.navigate('/home/menu');
                                          break;
                                        default:
                                          Modular.to
                                              .navigate('/home/quotation');
                                          break;
                                      }
                                    },
                                    isSelected: store.index == item.index,
                                  ))
                              .toList()))))),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class NavigationBarItem {
  String icon;
  String? title;
  int? index;

  NavigationBarItem(this.icon, {this.title, this.index});
}
