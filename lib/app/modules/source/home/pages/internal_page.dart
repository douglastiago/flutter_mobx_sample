import 'package:flutter/material.dart';

class InternalPage extends StatefulWidget {
  final String title;
  const InternalPage({Key? key, this.title = 'InternalPage'}) : super(key: key);
  @override
  InternalPageState createState() => InternalPageState();
}

class InternalPageState extends State<InternalPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(widget.title,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Poppins'))
                ])),
      ),
    );
  }
}
