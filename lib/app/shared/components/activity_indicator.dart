import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class ActivityIndicator extends StatefulWidget {
  const ActivityIndicator({Key? key}) : super(key: key);

  @override
  _ActivityIndicatorState createState() => _ActivityIndicatorState();
}

class _ActivityIndicatorState extends State<ActivityIndicator> {
  @override
  Widget build(BuildContext context) {
    return Lottie.asset('assets/animations/splash_fasttrade.json',
        width: 120, height: 120);
  }
}
