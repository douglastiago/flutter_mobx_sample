import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  final String? title;
  final Function()? onPressed;

  const ButtonWidget({Key? key, this.title, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(6)),
          ),
          fixedSize: Size(double.infinity, 48),
          primary: Colors.white,
          textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
          backgroundColor: Colors.blue),
      onPressed: onPressed,
      child: Container(alignment: Alignment.center, child: Text(title ?? '')),
    );
  }
}
