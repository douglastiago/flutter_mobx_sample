import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class TextFieldWidget extends StatefulWidget {
  final String? title;
  final IconData? icon;
  final bool isPassword;
  final int? maxLength;
  final TextInputType? keyboardType;
  final int? maxLine;
  final bool enabled;
  final Function()? onTap;
  final List<TextInputFormatter>? inputFormatters;
  final Function(String)? onChanged;
  final TextEditingController? controller;
  final Function(String)? onSubmitted;
  final FocusNode? focusNode;
  final TextInputAction action;

  const TextFieldWidget(
      {Key? key,
      this.title,
      this.icon,
      this.isPassword = false,
      this.onChanged,
      this.maxLength,
      this.maxLine,
      this.keyboardType = TextInputType.text,
      this.enabled = true,
      this.inputFormatters,
      this.controller,
      this.focusNode,
      this.onTap,
      this.onSubmitted,
      this.action = TextInputAction.done})
      : super(key: key);

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  bool _isFocused = false;
  bool _isObscure = false;

  Widget? _getIcon() {
    if (widget.isPassword) {
      return IconButton(
        icon: _isObscure
            ? SvgPicture.asset('assets/icons/icon_eye_opened.svg')
            : SvgPicture.asset('assets/icons/icon_eye_closed.svg'),
        onPressed: () => setState(() {
          _isObscure = !_isObscure;
        }),
      );
    } else if (widget.icon != null) {
      return Icon(
        widget.icon,
        color: Colors.white,
      );
    } else {
      return null;
    }
  }

  Widget _entryField() {
    return Container(
        padding: EdgeInsets.all(2),
        decoration: BoxDecoration(
            color: Color(0xFF1F2128),
            borderRadius: BorderRadius.all(
                Radius.circular(6.0) //                 <--- border radius here
                ),
            border: Border.all(
              width: 1,
              style: BorderStyle.solid,
              color: _isFocused ? Colors.blue : Color(0xFF1F2128),
            )),
        child: FocusScope(
            child: Focus(
                onFocusChange: (focus) {
                  setState(() {
                    _isFocused = focus;
                  });
                },
                child: TextField(
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                    controller: widget.controller,
                    textInputAction: widget.action,
                    enabled: widget.enabled,
                    onTap: widget.onTap,
                    onChanged: widget.onChanged,
                    obscureText: widget.isPassword && _isObscure,
                    keyboardType: widget.keyboardType,
                    maxLength: widget.maxLength,
                    maxLines: widget.maxLine ?? 1,
                    inputFormatters: widget.inputFormatters ?? [],
                    onSubmitted: widget.onSubmitted,
                    focusNode: widget.focusNode,
                    decoration: InputDecoration(
                        labelText: widget.title,
                        suffixIcon: _getIcon(),
                        labelStyle: TextStyle(
                          color: Colors.white,
                        ),
                        fillColor: Color(0xFF1F2128),
                        filled: true,
                        counterText: "",
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 6, horizontal: 16))))));
  }

  @override
  Widget build(BuildContext context) {
    return Container(margin: EdgeInsets.only(top: 10), child: _entryField());
  }
}
