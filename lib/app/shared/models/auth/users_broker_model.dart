class UsersBrokerModel {
  int? usersCode;
  String? usersBrokerName;
  String? usersBrokerLogoLogon;
  String? usersBrokerLogoMain;
  String? usersBrokerIP;
  String? usersBrokerConnPort;
  String? usersBrokerRiskPort;
  String? usersBrokerHistPort;
  String? usersBrokerIPCont;
  String? usersBrokerConnPortCont;
  String? usersBrokerRiskPortCont;
  String? usersBrokerHistPortCont;
  String? usersBrokerDBPort;
  String? usersBrokerDBPortCont;
  String? usersBrokerFaced;
  String? usersBrokerService;
  String? usersBrokerServiceSim;

  UsersBrokerModel(
      {this.usersCode,
      this.usersBrokerName,
      this.usersBrokerLogoLogon,
      this.usersBrokerLogoMain,
      this.usersBrokerIP,
      this.usersBrokerConnPort,
      this.usersBrokerRiskPort,
      this.usersBrokerHistPort,
      this.usersBrokerIPCont,
      this.usersBrokerConnPortCont,
      this.usersBrokerRiskPortCont,
      this.usersBrokerHistPortCont,
      this.usersBrokerDBPort,
      this.usersBrokerDBPortCont,
      this.usersBrokerFaced,
      this.usersBrokerService,
      this.usersBrokerServiceSim});

  static List<UsersBrokerModel> listFromMap(List<dynamic>? map) {
    List<UsersBrokerModel> brokers = [];
    for (var item in map ?? []) {
      brokers.add(UsersBrokerModel.fromMap(item));
    }
    return brokers;
  }

  static UsersBrokerModel fromMap(Map<String, dynamic>? map) {
    return UsersBrokerModel(
      usersCode: map?['usersCode'],
      usersBrokerName: map?['usersBrokerName'],
      usersBrokerLogoLogon: map?['usersBrokerLogoLogon'],
      usersBrokerLogoMain: map?['usersBrokerLogoMain'],
      usersBrokerIP: map?['usersBrokerIp'],
      usersBrokerConnPort: map?['usersBrokerConnPort'],
      usersBrokerRiskPort: map?['usersBrokerRiskPort'],
      usersBrokerHistPort: map?['usersBrokerHistPort'],
      usersBrokerIPCont: map?['usersBrokerIpCont'],
      usersBrokerConnPortCont: map?['usersBrokerConnPortCont'],
      usersBrokerRiskPortCont: map?['usersBrokerRiskPortCont'],
      usersBrokerHistPortCont: map?['usersBrokerHistPortCont'],
      usersBrokerDBPort: map?['usersBrokerDBPort'],
      usersBrokerDBPortCont: map?['usersBrokerDBPortCont'],
      usersBrokerFaced: map?['usersBrokerFaced'],
      usersBrokerService: map?['usersBrokerService'],
      usersBrokerServiceSim: map?['usersBrokerServiceSim'],
    );
  }
}
