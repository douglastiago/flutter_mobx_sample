class SignInModel {
  String? code;
  String? codeMessage;
  String? accountNumber;
  String? tokenWS;
  String? tokenWsGraphic;
  String? guidUser;
  String? token;
  String? expiration;
  String? message;

  SignInModel(
      {this.code,
      this.codeMessage,
      this.accountNumber,
      this.tokenWS,
      this.tokenWsGraphic,
      this.guidUser,
      this.token,
      this.expiration,
      this.message});

  static SignInModel fromMap(Map<String, dynamic>? map) {
    return SignInModel(
      code: map?['code'],
      codeMessage: map?['codeMessage'],
      accountNumber: map?['accountNumber'],
      tokenWS: map?['tokenWS'],
      tokenWsGraphic: map?['tokenWsGraphic'],
      guidUser: map?['guidUser'],
      token: map?['token'],
      expiration: map?['expiration'],
      message: map?['message'],
    );
  }
}
