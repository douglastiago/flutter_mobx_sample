import 'package:flutter_mobx_sample/app/shared/models/auth/software_model.dart';

import 'market_model.dart';

class PermissionsModel {
  List<MarketModel>? markets;
  List<SoftwareModel>? softwares;

  PermissionsModel({this.markets, this.softwares});

  static PermissionsModel fromMap(Map<String, dynamic>? map) {
    return PermissionsModel(
        markets: MarketModel.listFromMap(map?['markets']),
        softwares: SoftwareModel.listFromMap(map?['softwares']));
  }
}
