class ShadowModel {
  int? shadowCode;
  String? shadowLogin;
  String? shadowEmail;
  String? shadowAddress;
  String? shadowActive;
  String? shadowCpf;
  String? shadowName;
  String? shadowAdm;
  int? shadowConnectionQuantity;
  String? shadowSystem;
  String? shadowDemo;
  String? shadowProfessional;
  String? shadowInternational;
  int? usersCode;
  String? usersName;
  int? shadowTypeCode;
  String? shadowTypeDescription;
  int? bmfbovespaClassificationCode;
  String? bmfbovespaClassificationName;
  int? shadowConnectionTypeCode;
  String? shadowConnectionTypeDesc;
  int? allCountryCode;
  String? allCountryName;
  int? fasttradeVersionCode;
  String? fasttradeVersionDesc;

  ShadowModel(
      {this.shadowCode,
      this.shadowLogin,
      this.shadowEmail,
      this.shadowAddress,
      this.shadowActive,
      this.shadowCpf,
      this.shadowName,
      this.shadowAdm,
      this.shadowConnectionQuantity,
      this.shadowSystem,
      this.shadowDemo,
      this.shadowProfessional,
      this.shadowInternational,
      this.usersCode,
      this.usersName,
      this.shadowTypeCode,
      this.shadowTypeDescription,
      this.bmfbovespaClassificationCode,
      this.bmfbovespaClassificationName,
      this.shadowConnectionTypeCode,
      this.shadowConnectionTypeDesc,
      this.allCountryCode,
      this.allCountryName,
      this.fasttradeVersionCode,
      this.fasttradeVersionDesc});

  static ShadowModel fromMap(Map<String, dynamic>? map) {
    return ShadowModel(
      shadowCode: map?['shadowCode'],
      shadowLogin: map?['shadowLogin'],
      shadowEmail: map?['shadowEmail'],
      shadowAddress: map?['shadowAddress'],
      shadowActive: map?['shadowActive'],
      shadowCpf: map?['shadowCpf'],
      shadowName: map?['shadowName'],
      shadowAdm: map?['shadowAdm'],
      shadowConnectionQuantity: map?['shadowConnectionQuantity'],
      shadowSystem: map?['shadowSystem'],
      shadowDemo: map?['shadowDemo'],
      shadowProfessional: map?['shadowProfessional'],
      shadowInternational: map?['shadowInternational'],
      usersCode: map?['usersCode'],
      usersName: map?['usersName'],
      shadowTypeCode: map?['shadowTypeCode'],
      shadowTypeDescription: map?['shadowTypeDescription'],
      bmfbovespaClassificationCode: map?['bmfbovespaClassificationCode'],
      bmfbovespaClassificationName: map?['bmfbovespaClassificationName'],
      shadowConnectionTypeCode: map?['shadowConnectionTypeCode'],
      shadowConnectionTypeDesc: map?['shadowConnectionTypeDesc'],
      allCountryCode: map?['allCountryCode'],
      allCountryName: map?['allCountryName'],
      fasttradeVersionCode: map?['fasttradeVersionCode'],
      fasttradeVersionDesc: map?['fasttradeVersionDesc'],
    );
  }
}
