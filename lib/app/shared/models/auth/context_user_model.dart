import 'package:flutter_mobx_sample/app/shared/models/auth/general_message_model.dart';
import 'package:flutter_mobx_sample/app/shared/models/auth/permissions_model.dart';
import 'package:flutter_mobx_sample/app/shared/models/auth/shadow_model.dart';
import 'package:flutter_mobx_sample/app/shared/models/auth/users_broker_model.dart';

class ContextUserModel {
  int? code;
  ShadowModel? shadow;
  PermissionsModel? permissions;
  List<GeneralMessageModel>? generalMessages;
  List<UsersBrokerModel>? usersBroker;
  String? message;

  ContextUserModel(
      {this.code,
      this.shadow,
      this.permissions,
      this.generalMessages, // <-- Não validado
      this.usersBroker,
      this.message});

  static ContextUserModel fromMap(Map<String, dynamic>? map) {
    return ContextUserModel(
        code: map?['code'],
        shadow: ShadowModel.fromMap(map?['shadow']),
        permissions: PermissionsModel.fromMap(map?['permissions']),
        generalMessages:
            GeneralMessageModel.listFromMap(map?['generalMessages']),
        usersBroker: UsersBrokerModel.listFromMap(map?['usersBroker']),
        message: map?['message']);
  }
}
