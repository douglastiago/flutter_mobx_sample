class SoftwareModel {
  int? softwareCode;
  String? softwareName;
  String? softwareDescription;
  String? softwareKey;
  String? finalDate;
  double? expirationDays;

  SoftwareModel(
      {this.softwareCode,
      this.softwareName,
      this.softwareDescription,
      this.softwareKey,
      this.finalDate,
      this.expirationDays});

  static List<SoftwareModel> listFromMap(List<dynamic>? map) {
    List<SoftwareModel> softwares = [];
    for (var item in map ?? []) {
      softwares.add(SoftwareModel.fromMap(item));
    }
    return softwares;
  }

  static SoftwareModel fromMap(Map<String, dynamic>? map) {
    return SoftwareModel(
      softwareCode: map?['softwareCode'],
      softwareName: map?['softwareName'],
      softwareDescription: map?['softwareDescription'],
      softwareKey: map?['softwareKey'],
      finalDate: map?['finalDate'],
      expirationDays: map?['expirationDays'],
    );
  }
}
