class UserIPModel {
  final String? ip;

  UserIPModel({this.ip});

  static UserIPModel fromMap(Map<String, dynamic>? map) {
    return UserIPModel(ip: map?['ip']);
  }
}
