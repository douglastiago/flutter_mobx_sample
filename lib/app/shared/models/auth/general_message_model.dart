class GeneralMessageModel {
  int? generalMessageCode;
  String? message;
  String? generalMessageBeginDate;
  String? generalMessageFinalDate;

  GeneralMessageModel(
      {this.generalMessageCode,
      this.message,
      this.generalMessageBeginDate,
      this.generalMessageFinalDate});

  static List<GeneralMessageModel> listFromMap(List<dynamic>? map) {
    List<GeneralMessageModel> messages = [];
    for (var item in map ?? []) {
      messages.add(GeneralMessageModel.fromMap(item));
    }
    return messages;
  }

  static GeneralMessageModel fromMap(Map<String, dynamic>? map) {
    return GeneralMessageModel(
        generalMessageCode: map?['generalMessageCode'],
        message: map?['message'],
        generalMessageBeginDate: map?['generalMessageBeginDate'],
        generalMessageFinalDate: map?['generalMessageFinalDate']);
  }
}
