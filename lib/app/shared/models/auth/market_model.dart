class MarketModel {
  int? marketCode;
  String? marketName;
  String? marketDescription;
  String? marketDatabase;
  String? marketDelay;
  String? finalDate;

  MarketModel(
      {this.marketCode,
      this.marketName,
      this.marketDescription,
      this.marketDatabase,
      this.marketDelay,
      this.finalDate});

  static List<MarketModel> listFromMap(List<dynamic>? map) {
    List<MarketModel> markets = [];
    for (var item in map ?? []) {
      markets.add(MarketModel.fromMap(item));
    }
    return markets;
  }

  static MarketModel fromMap(Map<String, dynamic>? map) {
    return MarketModel(
        marketCode: map?['marketCode'],
        marketName: map?['marketName'],
        marketDescription: map?['marketDescription'],
        marketDatabase: map?['marketDatabase'],
        marketDelay: map?['marketDelay'],
        finalDate: map?['finalDate']);
  }
}
