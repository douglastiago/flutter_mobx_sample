class CustomException implements Exception {
  final String? _message;
  final Map<String, dynamic>? _data;

  CustomException([this._message, this._data]);

  @override
  String toString() {
    return _message ??
        "Desculpe, estamos com problemas técnicos. \nTente novamente!";
  }

  Map<String, dynamic>? getData() {
    return _data;
  }
}

class ServerException extends CustomException {
  ServerException()
      : super('Desculpe, estamos com problemas técnicos. \nTente novamente!');
}

class ConnectionException extends CustomException {
  ConnectionException() : super('Verifique sua conexão e atualize a página.');
}

class InputException extends CustomException {
  InputException([Map<String, dynamic>? data])
      : super(
            'Campos inválidos.\nVerifique os campos e tente novamente.', data);
}
