class ApiResponse<T> {
  T? data;
  bool error;
  String errorMessage;
  List<String>? errorInfo;

  ApiResponse(
      {this.data, this.error = false, this.errorMessage = "", this.errorInfo});
}
