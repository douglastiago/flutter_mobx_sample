import 'package:dio/dio.dart';
import 'package:flutter_mobx_sample/app/shared/models/auth/context_user_model.dart';
import 'package:flutter_mobx_sample/app/shared/models/auth/sign_in_model.dart';
import 'package:flutter_mobx_sample/app/shared/models/auth/user_ip_model.dart';
import 'package:flutter_mobx_sample/app/shared/repositories/base_repository.dart';
import 'package:flutter_mobx_sample/app/shared/repositories/utils/api_response.dart';
import 'package:flutter_modular/flutter_modular.dart';

@Injectable()
class AuthRepository extends BaseRepository {
  Future<ApiResponse<UserIPModel>> getUserIP() async {
    return get('https://api.ipify.org?format=json', customDioClient: Dio())
        .then((response) {
      return ApiResponse<UserIPModel>(data: UserIPModel.fromMap(response));
    }, onError: (error) {
      return ApiResponse<UserIPModel>(
          error: true, errorMessage: error.toString());
    });
  }

  Future<ApiResponse<ContextUserModel>> getContextInfoUser(
      Map<String, dynamic>? data) async {
    return post('/services/CedroServices/ContextInfoUser', data: data).then(
        (response) {
      return ApiResponse<ContextUserModel>(
          data: ContextUserModel.fromMap(response));
    }, onError: (error) {
      return ApiResponse<ContextUserModel>(
          error: true, errorMessage: error.toString());
    });
  }

  Future<ApiResponse<SignInModel>> signIn(Map<String, dynamic>? data) async {
    return post('/services/AuthFast/LoginSignIn', data: data).then((response) {
      return ApiResponse<SignInModel>(data: SignInModel.fromMap(response));
    }, onError: (error) {
      return ApiResponse<SignInModel>(
          error: true, errorMessage: error.toString());
    });
  }
}
