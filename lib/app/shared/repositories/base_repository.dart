import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_mobx_sample/app/shared/repositories/utils/custom_exception.dart';
import 'package:flutter_modular/flutter_modular.dart';

class BaseRepository extends Disposable {
  final Dio defaultDioClient = Modular.get();

  Future<dynamic> send(Function request) async {
    try {
      var response = await request();
      return response.data;
    } on DioError catch (e) {
      if (e.error is SocketException) {
        throw ConnectionException();
      }

      if (e.response?.statusCode == 422) {
        throw InputException(e.response?.data ?? <String, dynamic>{});
      }
      throw ServerException();
    }
  }

  Future<dynamic> get(String endpoint,
      {Map<String, dynamic>? queryParameters, Dio? customDioClient}) {
    var dioClient = customDioClient ?? defaultDioClient;
    return send(
        () => dioClient.get(endpoint, queryParameters: queryParameters));
  }

  Future<dynamic> post(String endpoint,
      {data, Map<String, dynamic>? queryParameters, Dio? customDioClient}) {
    var dioClient = customDioClient ?? defaultDioClient;
    return send(() =>
        dioClient.post(endpoint, data: data, queryParameters: queryParameters));
  }

  Future<dynamic> put(String endpoint,
      {data, Map<String, dynamic>? queryParameters, Dio? customDioClient}) {
    var dioClient = customDioClient ?? defaultDioClient;
    return send(() =>
        dioClient.put(endpoint, data: data, queryParameters: queryParameters));
  }

  Future<dynamic> patch(String endpoint,
      {data, Map<String, dynamic>? queryParameters, Dio? customDioClient}) {
    var dioClient = customDioClient ?? defaultDioClient;
    return send(() => dioClient.patch(endpoint,
        data: data, queryParameters: queryParameters));
  }

  Future<dynamic> delete(String endpoint,
      {data, Map<String, dynamic>? queryParameters, Dio? customDioClient}) {
    var dioClient = customDioClient ?? defaultDioClient;
    return send(() => dioClient.delete(endpoint,
        data: data, queryParameters: queryParameters));
  }

  @override
  void dispose() {}
}
