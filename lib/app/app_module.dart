import 'package:dio/dio.dart';
import 'package:flutter_mobx_sample/app/modules/source/home/home_module.dart';
import 'package:flutter_mobx_sample/app/shared/repositories/auth_repository.dart';
import 'package:flutter_mobx_sample/app/modules/auth/auth_module.dart';
import 'package:flutter_mobx_sample/app/modules/splash_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => AuthRepository()),
    Bind((i) => Dio(BaseOptions(
            baseUrl: 'https://web.plataformafasttrade.com.br/facade',
            headers: {
              'Content-type': 'application/json',
              'Accept': 'application/json',
            })))
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, args) => SplashPage()),
    ModuleRoute('/auth',
        module: AuthModule(),
        transition: TransitionType.fadeIn,
        duration: Duration(milliseconds: 800)),
    ModuleRoute('/home', module: HomeModule())
  ];
}
